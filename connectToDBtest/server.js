// monogooes connection
const mongoose = require('mongoose');
const { stringify } = require('querystring');

async function main(){
  await mongoose.connect('mongodb://127.0.0.1:27017/test');
}
main().then(
  ()=>{
    console.log("Connected Successfully")
  }
).catch(
  (e)=>{
    console.log(e)
  }
)
// scghema defined
const userSchema = new mongoose.Schema({
  name : String,
  email: String,
  age : Number
})

const User=mongoose.model("User",userSchema);
 
//insert one
const user1 =new User({name:"Nikhi",email:"abc@gmail.com",age:21})
//user1.save()
// insert many 
// User.insertMany([
//   {name:"Lucky",email:"lucky@gamil.com",age:20},
//   {name:"Amit",email:"amit@gamil.com",age:21},
//   {name:"Pranva",email:"pranva@gamil.com",age:22},
// ]).then(
//   (res)=>{
//     console.log(res)
//   }
// ).catch((e)=> console.log(e) )

// find  {age :{$gte : 21}
// User.find({}).then(
//     (res)=>{
//       console.log(res)
//     }
//   ).catch((e)=> console.log(e) )
// User.updateOne({name : "Pranva"}, {age : 23}).then(
//       (res)=>{
//         console.log(res)
//       }
//     ).catch((e)=> console.log(e) )


//schema validation

const bookSchema =new mongoose.Schema({
  title :{
    type: String,
    required:true,
    maxLength : 20
  },
  author:{
    type : String 
  },
  price: {
    type :Number,
    min : [1,"please enter the correct price"]
  },
  geners : [String],
  
   category : {
    type : String,
    enum : ["fiction","Action","Horror"]

  }
})

