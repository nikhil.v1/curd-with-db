const products = require("./model/productSchema.js");
const mongoose = require("mongoose");

async function main() {
  await mongoose.connect("mongodb://127.0.0.1:27017/productsDB");
}
main()
  .then(() => {
    console.log("Connected Successfully");
  })
  .catch((e) => {
    console.log(e);
  });

const allProducts = [
  {
    p_Name: "Product 1",
    p_Price: 29.99,
    p_Info: "A great product with all features.",
    p_rating: 4.5,
  },
  {
    p_Name: "Product 2",
    p_Price: 49.99,
    p_Info: "High-quality product with excellent reviews.",
    p_rating: 4.8,
  },
  {
    p_Name: "Product 3",
    p_Price: 19.99,
    p_Info: "Affordable product for everyday use.",
    p_rating: 3.9,
  },
  {
    p_Name: "Product 4",
    p_Price: 99.99,
    p_Info: "Premium product with top-notch features.",
    p_rating: 5.0,
  },
  {
    p_Name: "Product 5",
    p_Price: 39.99,
    p_Info: "Good quality product at a reasonable price.",
    p_rating: 4.2,
  },
];

products.insertMany(allProducts);
