const mongoose = require("mongoose");

const productScheme = {
  p_Name: {
    type: String,
    required: true,
  },
  p_Price: {
    type: Number,
    required: true,
  },
  p_Info: {
    type: String,
    required: true,
    maxLength: 50,
  },
  p_rating: {
    type: Number,
    required:true
  }
};

const products = mongoose.model("products", productScheme);

module.exports = products;
