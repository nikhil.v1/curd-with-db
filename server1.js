//product app model curd

const express = require("express");
const app = express();
const path = require("path");
const products = require("./model/productSchema.js");
const mongoose = require("mongoose")
var methodOverride = require('method-override')
require("dotenv").config()
app.use(methodOverride('_method'))

const url =process.env.URL

// async function main() {
//     await mongoose.connect("mongodb://127.0.0.1:27017/productsDB");
//   }
//   main()
//     .then(() => {
//       console.log("Connected Successfully");
//     }) 
//     .catch((e) => {
//       console.log(e);
//     });


async function connectWithRetry(attempts = 5, delay = 2000) {
  let lastError;
  for (let i = 0; i < attempts; i++) {
    try {
      await mongoose.connect(url);
      console.log("Connected Successfully");
      return; // Exit function after successful connection
    } catch (error) {
      lastError = error;
      console.log(`Connection attempt ${i + 1} failed. Retrying in ${delay / 1000} seconds...`);
      if (i < attempts - 1) {
        await new Promise(resolve => setTimeout(resolve, delay)); // Wait for the specified delay before retrying
      }
    }
  }

  // If all attempts fail, log the error
  console.error("All connection attempts failed.");
  console.error(lastError);
}

connectWithRetry()
  .catch((e) => console.log("Unexpected error:", e));

const port = 3000;

app.use(express.urlencoded({extended:true}))

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname,"public")))

app.get('/products',async (req,res)=>{
    let D_product = await products.find()
   // console.log(D_product)
  res.render("index.ejs",{D_product})
 // res.send("working")

})

// new route
app.get('/products/addProduct',(req,res)=>{

  res.render("addProduct.ejs")
})

//crete route
app.post('/products',(req,res)=>{
  let {p_Name , p_rating ,  p_Price,p_Info}=req.body
  let newProduct = new products({
    p_Name:p_Name,
    p_Price:p_Price,
    p_Info:p_Info,
    p_rating:p_rating
  })
 //console.log(newProduct)
 newProduct.save()
  res.redirect("/products")
 // res.send("working")

})

app.get('/products/:id/product',async (req,res)=>{
    let {id}=req.params
    let singleProduct = await products.findById(id)
  // console.log("id single ",singleProduct)
    res.render("edit",{singleProduct})
})



app.put('/products/:id',async (req,res)=>{
  let {id}=req.params
  let {p_Name : Up_Name , p_rating: Up_rating ,  p_Price :Up_Price,p_Info : Up_info}=req.body
 // console.log("update req",req.body)
  let updateProduct = await products.findByIdAndUpdate(id,{
    p_Name:Up_Name,
    p_Price:Up_Price,
    p_Info:Up_info,
    p_rating:Up_rating
  },{runValidators:true , new : true})
 // console.log("after update",updateProduct)
  res.redirect("/products")
})
 

app.delete('/products/:id',async (req,res)=>{
  let {id}=req.params
  let deleteproduct = await products.findByIdAndDelete(id)
 // console.log(deleteproduct)
  res.redirect("/products")
})




app.get("/", (req, res) => res.send("Hello World!"));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
